import pytest
import allure
import common_functions as cf
from settings import paths
from settings.conf import CREATE_EMP_URL, CREATE_EMP_HEADERS
from tc_data.v1_get_employer import V1_GET_EMPLOYER


@allure.feature('v1 api')
@allure.story('Get_employer_data')
@pytest.mark.parametrize("case", V1_GET_EMPLOYER.values(),
                         ids=[name for name in V1_GET_EMPLOYER.keys()])
def test_v1_get_employer(case):
    """get employer data with response validation
    + checking age and salary"""
    req = cf.request('get', f"{CREATE_EMP_URL}/public/api/v1/employee/{case.id}",
                     headers=CREATE_EMP_HEADERS)

    with allure.step('Analyze results'):
        cf.validation(req.json(), paths.JSON_SCHEMA_GET_EMPLOYER)
        assert req.status_code == case['expected']["status_code"]
        assert case.check_result(req.json())
