from os.path import join, pardir, dirname, abspath, normpath

current = abspath(dirname(__file__))
DUMMY_API = normpath(join(current, pardir))

JSON_SCHEMA = join(DUMMY_API, 'json_schema')
SCHEMA_CREATE_EMPLOYER_SUCCESS = join(JSON_SCHEMA, 'create_employer_success_schema.json')
SCHEMA_CREATE_EMPLOYER_ERROR = join(JSON_SCHEMA, 'create_employer_error_schema.json')
JSON_SCHEMA_GET_EMPLOYER = join(JSON_SCHEMA, 'get_employer_data_schema.json')