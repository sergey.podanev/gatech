from faker import Faker

fake = Faker()

TEMPLATE_200 = {
    "status_code": 200,
    "status": "success",
    "message": "Successfully! Record has been added."
}

V1_CREATE_EMPLOYER = {
    'base creation':
        {'url': '/api/v1/create',
         'payload':
             {
                 "name": fake.first_name(),
                 "salary": 99999,
                 "age": "23"
             },
         'expected': TEMPLATE_200
         },
    'wrong_age_NEGATIVE': {
        'url': '/api/v1/create',
        'payload':
            {
                "name": fake.first_name(),
                "salary": "123456",
                "age": -1
            },
        'expected': {
            "status_code": 400}},
    'zero_salary_NEGATIVE':
        {'url': '/api/v1/create',
         'payload':
             {
                 "name": fake.first_name(),
                 "salary": "0",
                 "age": "99"
             },
         'expected': {
             "status_code": 400}}
}
