from settings import paths
from dataclasses import dataclass
from typing import Optional

success_message = "Successfully! Record has been added."
no_found_message = "Not found record"


@dataclass
class GetEmployer:
    id: int
    status_code: int
    message: str
    employee_name: Optional[str] = None
    employee_salary: Optional[int] = None
    employee_age: Optional[int] = None
    schema: str = paths.SCHEMA_CREATE_EMPLOYER_SUCCESS

    def check_result(self, data: dict):
        for attr in data:
            assert getattr(self, attr) == data[attr]
        return True


V1_GET_EMPLOYER = {
    'id=1': GetEmployer(id=1, status_code=200, employee_name='Tiger Nixon',
                        employee_salary=320800, employee_age=61,
                        message=success_message)
    ,
    'id=0_negative': GetEmployer(0, status_code=400, message=no_found_message,
                                 schema=paths.SCHEMA_CREATE_EMPLOYER_ERROR),
    'id=-1_negative': GetEmployer(-1, status_code=400, message=no_found_message,
                                  schema=paths.SCHEMA_CREATE_EMPLOYER_ERROR)
}
