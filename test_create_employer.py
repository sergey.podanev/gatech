import pytest
import allure
import common_functions as cf
from tc_data.v1_create_employer import V1_CREATE_EMPLOYER
from settings.conf import CREATE_EMP_URL, CREATE_EMP_HEADERS


@allure.feature('v1 api')
@allure.story('Create_employer')
@pytest.mark.parametrize("case", V1_CREATE_EMPLOYER.values(), ids=list(V1_CREATE_EMPLOYER.keys()))
def test_v1_create_employer(case):
    """creating employer base case with response validation"""
    req = cf.request('post', CREATE_EMP_URL + case['url'], data=case['payload'],
                     headers=CREATE_EMP_HEADERS)
    result = req.json()
    with allure.step('Analyze results'):
        assert req.status_code == case['expected']["status_code"]
        assert result['message'] == case["message"]
        cf.validation(req.json(), case['schema'])
