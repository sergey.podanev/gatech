import allure
import json
import requests
from jsonschema import validate


@allure.step("Json validation")
def validation(data, schema_path):
    """Json validation by schema"""
    with open(schema_path) as file:
        schema = json.load(file)
        allure.attach("jsonvalidator.json",
                      json.dumps(schema, indent=4),
                      attachment_type="JSON")
    validate(data, schema)


def request(method, url, **kwargs):
    with allure.step(f'{method}  {url}'):
        req = requests.request(method, url, **kwargs)
        allure.attach(req.text,
                      'response',
                      attachment_type=allure.attachment_type.TEXT)
    return req
